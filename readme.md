# Different Sums Per Sort in esProc SPL IDE

![DiffSumPerSort.png](./DiffSumPerSort.png?raw=true "Screenshot")
## About

Getting incomplete totals when open a file with file() vs T(), and different totals using file() depending on how the input is sorted.

## Home

<https://www.scudata.com/>

## Tutorial

<http://doc.scudata.com/>)

## Community

<http://c.scudata.com/>

## Function Reference

<http://doc.scudata.com/esproc/func/>

## Sample Data

data1.txt

## Program

diff_sum_per_sort.splx
