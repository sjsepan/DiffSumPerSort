EID	NAME	SURNAME	GENDER	STATE	BIRTHDAY	HIREDATE	DEPT	SALARY
1	Rebecca	Moore	F	California	1984-09-28	2015-01-18	R&D	7000
2	Ashley	Wilson	F	New York	1990-05-28	2018-01-23	Finance	11000
3	Rachel	Johnson	F	New Mexico	1980-10-25	2020-10-09	Sales	9000
4	Emily	Smith	F	Texas	1995-01-14	2016-06-23	HR	7000
5	Ashley	Smith	F	Texas	1985-03-21	2014-06-08	R&D	16000
6	Matthew	Johnson	M	California	1994-05-16	2015-05-16	Sales	11000
7	Alexis	Smith	F	Illinois	1982-06-25	2012-06-24	Sales	9000
8	Megan	Wilson	F	California	1989-02-25	2014-02-26	Marketing	11000
9	Victoria	Davis	F	Texas	1993-10-15	2019-10-16	HR	3000